title: GitLab on Red Hat
canonical_path: /partners/technology-partners/redhat/
seo_title: GitLab on Red Hat OpenShift
description: Scale faster and code better together with a complete, open DevOps platform to build, test, and deploy on Red Hat OpenShift.
title_description: >-
      Scale faster and code better together with a complete, open DevOps platform to build, test, and deploy on Red Hat OpenShift.

      Red Hat and GitLab have invested in our joint partnership to be able to deliver the best possible experience for those customers who choose GitLab and Red Hat for helping to modernize their IT infrastructure. Customers can be assured of having a best in class solution for their DevOps environments as a result of the joint testing and [Red Hat certification for the GitLab Runner](https://catalog.redhat.com/software/operators/search?q=GitLab).
logo: /images/logos/redhat_logo_sqaure.svg
logo_alt: Red Hat Logo
body_top_title: From big ideas to deploying on Kubernetes
body_top_description: Iterate faster and innovate together using a single solution for everyone on your pipeline. GitLab is an end-to-end source code management (SCM) and continuous integration (CI) solution for scaling modern applications on Red Hat OpenShift. Leverage GitLab's tight Kubernetes integration to simplify containerized workload deployments on Red Hat OpenShift Container Platform. 
body_top_cta_1_text: Read the blog
body_top_cta_2_text: Talk to an expert
body_top_cta_img_1: "/images/topics/featured-partner-redhat-1.png"
body_top_cta_img_2: "/images/topics/featured-partner-redhat-2.svg"
body_top_cta_1_url: /blog/2020/04/29/gitlab-and-redhat-automation/
body_top_cta_2_url: /sales/
cta_whitepaper_banner:
  - title: DevOps and Cloud Native App Development
    body: >-
      Deploying GitLab and Red Hat OpenShift together supports an organization’s journey to DevOps and cloud native application development, while delivering the powerful CI/CD platform needed to build and run your applications.  Software delivery cycle times are collapsed with higher efficiency across all stages of the development lifecycle.

      
      [Learn more](https://www.openshift.com/try) about Red Hat OpenShift and how to [install the GitLab operator](https://www.openshift.com/blog/installing-the-gitlab-runner-the-openshift-way). 
body_features_title: Develop better cloud native applications faster with GitLab and Red Hat OpenShift
body_features_description: Shrink cycle times and expand possibilities by driving efficiency at every stage of your software development process with GitLab. Unlock built-in planning, monitoring, and reporting solutions to enable and secure your public, private, or hybrid cloud deployments on Red Hat OpenShift. 
body_quote: GitLab can accelerate software development and deployment of applications while Red Hat Enterprise Linux can act as the more secure, fully managed OS that can scale with the application. The inclusion of new DevOps tools in Red Hat’s hybrid cloud technologies, like service mesh, empowers developers to iterate faster on a foundation of trusted enterprise Linux.
body_quote_source: Vick Kelkar, Director of Alliances at GitLab
body_quote_2: Red Hat's partner ecosystem is a vital component to delivering innovative, flexible and open solutions. We are pleased to collaborate with GitLab to build, test and certify GitLab Runner in order to help joint customers modernize IT infrastructure and support on-premise and multi-cloud environments.
body_quote_source_2: Mike Werner, Senior Director, Global Ecosystems, Red Hat
feature_items:
  - title: "Creative contributions"
    description: "Code what matters. Version control and collaboration reduce rework so happier developers can expand product roadmaps instead of repairing old roads."
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
  - title: "Worry-free workflows"
    description: "Armor your automation. Increase uptime by reducing security and compliance risks on public, private, and hybrid clouds. "
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
  - title: "Reliable results"
    description: "Succeed, repeat. Increase market share and revenue when your product is on budget, on time, and always up. "
    icon: "/images/enterprise/gitlab-enterprise-icon-smiley-face.svg"
body_middle_title: "Get started with GitLab and Red Hat Joint Solutions"
body_middle_description: >-
  No matter where you’re at in your development journey, GitLab gets it. As an open core platform, GitLab gives you the freedom to maintain the investment in your current toolchain as you modernize. Cloud native developers prefer GitLab’s hybrid cloud CI/CD pipeline and rely on its multicloud strategy with workflow portability to increase operational efficiencies.
body_middle_items:
  - title: "Red Hat Enterprise Linux"
    description: "Deploy Red Hat Enterprise Linux, the world’s leading enterprise-grade Linux operating system (OS) platform, across the hybrid cloud, from bare metal and virtual servers to private and public cloud environments. Red Hat Enterprise Linux makes it easier for operations teams to manage the upgrades, security patches, and lifecycles of servers that run applications like GitLab."
    icon: "/images/icons/icon-1.svg"
    link: "https://www.redhat.com/en/enterprise-linux-8"
  - title: "Red Hat OpenShift Container Platform (OCP)"
    description: "Red Hat provides the industry’s most comprehensive enterprise Kubernetes platform in Red Hat OpenShift. OpenShift is uniquely positioned to run containerized applications on public or private clouds. GitLab is a Certified OpenShift Operator, providing Day-1 and Day-2 operations via GitLab Runner."
    icon: "/images/icons/icon-2.svg"
    link: "https://www.openshift.com/try"
  - title: "Red Hat Ansible"
    description: "GitLab enables Infrastructure as Code (IaC) with Ansible. Trigger automation with source control: apply infrastructure configuration changes, deploy new services, reconfigure existing applications, and more. Use GitLab CI to edit and ship code from the Ansible playbook without installing local dependencies."
    icon: "/images/icons/icon-3.svg"
    link: "https://www.redhat.com/en/technologies/management/ansible"
benefits_title: Discover the benefits of GitLab on Red Hat OpenShift
featured_video_title: "Opening Keynote: The Power of GitLab - Sid Sijbrandij"
featured_video_url: "https://www.youtube-nocookie.com/embed/xn_WP4K9dl8"
featured_video_topic: "GitLab Commit Virtual 2020"
featured_video_more_url: "https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg/featured"
resources:
  - title: GitLab and Red Hat OpenShift- Automation to enhance secure software development
    url: /blog/2020/04/29/gitlab-and-redhat-automation/
    type: Blog
  - title: Installing the GitLab Runner the OpenShift Way
    url: https://www.openshift.com/blog/installing-the-gitlab-runner-the-openshift-way
    type: Blog
  - title: GitLab Runner on Red Hat Openshift
    url: https://www.youtube.com/watch?v=5AbtSxpFQec&feature=youtu.be
    type: Video
cta_banner:
  - title: How can GitLab integrate with your Red Hat OpenShift infrastructure?
    button_url: https://about.gitlab.com/sales/
    button_text: Talk to an expert       
